<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity as AssertUniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource()
 * @AssertUniqueEntity({"bond", "date"})
 * @ORM\Entity(repositoryClass="App\Repository\AmortizationRepository")
 * @ORM\Table(
 *     uniqueConstraints={
 *      @ORM\UniqueConstraint(name="amortization_date_value", columns={"date", "value"})
 *     }
 * )
 */
class Amortization
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ApiProperty(push=true)
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="App\Entity\Bond", inversedBy="amortizations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $bond;

    /**
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Date()
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\GreaterThan(0)
     * @ORM\Column(type="float")
     */
    private $value;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBond(): ?Bond
    {
        return $this->bond;
    }

    public function setBond(?Bond $bond): self
    {
        $this->bond = $bond;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getValue(): ?float
    {
        return $this->value;
    }

    public function setValue(float $value): self
    {
        $this->value = $value;

        return $this;
    }
}
