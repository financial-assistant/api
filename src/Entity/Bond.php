<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\ExistsFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity as AssertUniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource()
 * @AssertUniqueEntity("ISIN")
 * @ORM\Entity(repositoryClass="App\Repository\BondRepository")
 * @ORM\Table(
 *     uniqueConstraints={
 *      @ORM\UniqueConstraint(name="isinx", columns={"isin"})
 *     }
 * )
 * @ApiFilter(
 *     OrderFilter::class,
 *     properties={
 *      "rating.yearlyRevenue"
 *    }
 * )
 * @ApiFilter(
 *     ExistsFilter::class,
 *     properties={
 *      "rating"
 *     }
 * )
 */
class Bond
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Type("string")
     * @Assert\Regex("/^[A-Z]{2}[A-Z\d]{9}\d$/")
     * @ORM\Column(type="string", length=12)
     */
    private $ISIN;

    /**
     * @ApiProperty(push=true)
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="App\Entity\Issuer", inversedBy="bonds")
     * @ORM\JoinColumn(nullable=false)
     */
    private $issuer;

    /**
     * @ApiProperty(push=true)
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="App\Entity\Currency")
     * @ORM\JoinColumn(nullable=false)
     */
    private $currency;

    /**
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @Assert\Date()
     * @ORM\Column(type="date", nullable=true)
     */
    private $offerEnd;

    /**
     * @Assert\Date()
     * @ORM\Column(type="date", nullable=true)
     */
    private $maturity;

    /**
     * @Assert\Type("float")
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\GreaterThan(0)
     * @ORM\Column(type="float")
     */
    private $faceValue;

    /**
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\GreaterThanOrEqual(0)
     * @ORM\Column(type="bigint")
     */
    private $quantity;

    /**
     * @Assert\GreaterThan(0)
     * @ORM\Column(type="float", nullable=true)
     */
    private $price;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Amortization", mappedBy="bond", orphanRemoval=true)
     */
    private $amortizations;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Coupon", mappedBy="bond", orphanRemoval=true)
     */
    private $coupons;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\BondRating", mappedBy="bond", cascade={"persist", "remove"})
     */
    private $rating;

    public function __construct()
    {
        $this->amortizations = new ArrayCollection();
        $this->coupons = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getISIN(): ?string
    {
        return $this->ISIN;
    }

    public function setISIN(string $ISIN): self
    {
        $this->ISIN = $ISIN;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getOfferEnd(): ?\DateTimeInterface
    {
        return $this->offerEnd;
    }

    public function setOfferEnd(?\DateTimeInterface $offerEnd): self
    {
        $this->offerEnd = $offerEnd;

        return $this;
    }

    public function getMaturity(): ?\DateTimeInterface
    {
        return $this->maturity;
    }

    public function setMaturity(?\DateTimeInterface $maturity): self
    {
        $this->maturity = $maturity;

        return $this;
    }

    public function getFaceValue(): ?float
    {
        return $this->faceValue;
    }

    public function setFaceValue(float $faceValue): self
    {
        $this->faceValue = $faceValue;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(?float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getIssuer(): ?Issuer
    {
        return $this->issuer;
    }

    public function setIssuer(?Issuer $issuer): self
    {
        $this->issuer = $issuer;

        return $this;
    }

    public function getCurrency(): ?Currency
    {
        return $this->currency;
    }

    public function setCurrency(?Currency $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * @return Collection|Amortization[]
     */
    public function getAmortizations(): Collection
    {
        return $this->amortizations;
    }

    public function addAmortization(Amortization $amortization): self
    {
        if (!$this->amortizations->contains($amortization)) {
            $this->amortizations[] = $amortization;
            $amortization->setBond($this);
        }

        return $this;
    }

    public function removeAmortization(Amortization $amortization): self
    {
        if ($this->amortizations->contains($amortization)) {
            $this->amortizations->removeElement($amortization);
            // set the owning side to null (unless already changed)
            if ($amortization->getBond() === $this) {
                $amortization->setBond(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Coupon[]
     */
    public function getCoupons(): Collection
    {
        return $this->coupons;
    }

    public function addCoupon(Coupon $coupon): self
    {
        if (!$this->coupons->contains($coupon)) {
            $this->coupons[] = $coupon;
            $coupon->setBond($this);
        }

        return $this;
    }

    public function removeCoupon(Coupon $coupon): self
    {
        if ($this->coupons->contains($coupon)) {
            $this->coupons->removeElement($coupon);
            // set the owning side to null (unless already changed)
            if ($coupon->getBond() === $this) {
                $coupon->setBond(null);
            }
        }

        return $this;
    }

    public function getRating(): ?BondRating
    {
        return $this->rating;
    }

    public function setRating(?BondRating $rating): self
    {
        $this->rating = $rating;

        // set (or unset) the owning side of the relation if necessary
        $newBond = $rating === null ? null : $this;
        if ($newBond !== $rating->getBond()) {
            $rating->setBond($newBond);
        }

        return $this;
    }
}
