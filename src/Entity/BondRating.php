<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\BondRatingRepository")
 */
class BondRating
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Bond", inversedBy="rating", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $bond;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $yearlyPriceRevenue;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $yearlyCouponsRevenue;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $yearlyRevenue;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBond(): ?Bond
    {
        return $this->bond;
    }

    public function setBond(Bond $bond): self
    {
        $this->bond = $bond;

        return $this;
    }

    public function getYearlyPriceRevenue(): ?float
    {
        return $this->yearlyPriceRevenue;
    }

    public function setYearlyPriceRevenue(?float $yearlyPriceRevenue): self
    {
        $this->yearlyPriceRevenue = $yearlyPriceRevenue;

        return $this;
    }

    public function getYearlyCouponsRevenue(): ?float
    {
        return $this->yearlyCouponsRevenue;
    }

    public function setYearlyCouponsRevenue(?float $yearlyCouponsRevenue): self
    {
        $this->yearlyCouponsRevenue = $yearlyCouponsRevenue;

        return $this;
    }

    public function getYearlyRevenue(): ?float
    {
        return $this->yearlyRevenue;
    }

    public function setYearlyRevenue(?float $yearlyRevenue): self
    {
        $this->yearlyRevenue = $yearlyRevenue;

        return $this;
    }
}
