<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Enum\IssuerType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity as AssertUniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource()
 * @AssertUniqueEntity("name")
 * @ORM\Entity(repositoryClass="App\Repository\IssuerRepository")
 */
class Issuer
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Type("string")
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Type("int")
     * @Assert\Choice(callback={IssuerType::class, "toArray"})
     * @ORM\Column(type="php_enum_issuer_type")
     */
    private $type;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Bond", mappedBy="issuer", orphanRemoval=true)
     */
    private $bonds;

    public function __construct()
    {
        $this->bonds = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection|Bond[]
     */
    public function getBonds(): Collection
    {
        return $this->bonds;
    }

    public function addBond(Bond $bond): self
    {
        if (!$this->bonds->contains($bond)) {
            $this->bonds[] = $bond;
            $bond->setIssuer($this);
        }

        return $this;
    }

    public function removeBond(Bond $bond): self
    {
        if ($this->bonds->contains($bond)) {
            $this->bonds->removeElement($bond);
            // set the owning side to null (unless already changed)
            if ($bond->getIssuer() === $this) {
                $bond->setIssuer(null);
            }
        }

        return $this;
    }
}
