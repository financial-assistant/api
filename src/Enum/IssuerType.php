<?php
declare(strict_types=1);

namespace App\Enum;

use MyCLabs\Enum\Enum;

/**
 * A list of possible types for the issuer.
 */
class IssuerType extends Enum
{
    /**
     * @var int Государство
     */
    public const STATE = 1;

    /**
     * @var int Регион/штат государства
     */
    public const MUNICIPAL = 2;

    /**
     * @var int Частная компания
     */
    public const CORPORATE = 3;
}
