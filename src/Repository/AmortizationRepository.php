<?php

namespace App\Repository;

use App\Entity\Amortization;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Amortization|null find($id, $lockMode = null, $lockVersion = null)
 * @method Amortization|null findOneBy(array $criteria, array $orderBy = null)
 * @method Amortization[]    findAll()
 * @method Amortization[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AmortizationRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Amortization::class);
    }
}
