<?php

namespace App\Repository;

use App\Entity\BondRating;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method BondRating|null find($id, $lockMode = null, $lockVersion = null)
 * @method BondRating|null findOneBy(array $criteria, array $orderBy = null)
 * @method BondRating[]    findAll()
 * @method BondRating[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BondRatingRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, BondRating::class);
    }
}
