<?php

namespace App\Repository;

use App\Entity\Bond;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Bond|null find($id, $lockMode = null, $lockVersion = null)
 * @method Bond|null findOneBy(array $criteria, array $orderBy = null)
 * @method Bond[]    findAll()
 * @method Bond[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BondRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Bond::class);
    }
}
