<?php

namespace App\Type;

use App\Enum\IssuerType;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\SmallIntType;
use InvalidArgumentException;

class IssuerTypeEnumType extends SmallintType
{
    /**
     * {@inheritdoc}
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if (!IssuerType::isValid($value)) {
            throw new InvalidArgumentException(
                sprintf(
                    'The value "%s" is not valid for the enum "%s". Expected one of ["%s"]',
                    $value,
                    IssuerType::class,
                    implode('", "', IssuerType::keys())
                )
            );
        }

        return parent::convertToPHPValue($value, $platform);
    }

    /**
     * Gets the name of this type.
     *
     * @return string
     */
    public function getName(): string
    {
        return 'php_enum_issuer_type';
    }
}
